import 'package:flutter/material.dart';
import 'package:naadam_app/tabs/HomeTab.dart';

class HomeScreen extends StatefulWidget
{
  static const String routeName = "/home";
  HomeScreen({Key key, this.title}) : super(key: key);
  final String title;
  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen>  with TickerProviderStateMixin
{
  TabController controller;

  @override
  void initState()
  {
    super.initState();
    controller = new TabController(length: 4, vsync: this);
  }

  @override
  void dispose()
  {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context)
  {
    // TODO: implement build
    return Scaffold(
      // Appbar
      appBar: new AppBar(
        // Title
        title: Text("Home"),
        // Set the background color of the App Bar
        backgroundColor: Colors.blue,
      ),
      // Set the TabBar view as the body of the Scaffold
      body: new TabBarView(
        // Add tabs as widgets
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>
        [
          new HomeTab(),
          new HomeTab(),
        ],
        // set the controller
        controller: controller,
      ),
      // Set the bottom navigation bar
      bottomNavigationBar: new Material(
        // set the color of the bottom navigation bar
        color: Colors.blue,
        // set the tab bar as the child of bottom navigation bar
        child: new TabBar(
          tabs: <Tab>[
            new Tab(
              // set icon to the tab
                icon: new Icon(Icons.home),
                text : "Нүүр"
            ),
            new Tab(
              icon: new Icon(Icons.schedule),
                text : "Хуваарь"
            )
          ],
          // setup the controller
          controller: controller,
        ),
      ),
    );
  }

}