import 'package:flutter/material.dart';

class HomeTab extends StatefulWidget
{
  @override
  HomeTabState createState() => HomeTabState();
}

class HomeTabState extends State<HomeTab>
{

  void initState()
  {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      //backgroundColor: Colors.red,
      body : Container(
        child: makeCard(context),
      ),
    );
  }

  Card makeCard (BuildContext context)
  {
    return new Card(
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/home.jpeg"),
            fit: BoxFit.fitWidth,
            alignment: Alignment.topCenter,
          ),
        ),
        child: Text("YOUR TEXT"),
      ),
    );
  }
}
